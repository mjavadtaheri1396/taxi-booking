import psycopg2
import array
conn = psycopg2.connect("dbname=taxi_booking user=postgres")
cur = conn.cursor()
conn.commit()


def first_menu():
    print('Welcome to our online taxi booking system')
    print('If you have an account you can sign in to it')
    print('otherwise it is possible for you to create an account')
    print('which of these described is your choice? \n1.sign up\t2.sig in')
    first_decision = input('please enter number of your choice: ')
    if first_decision == '1':
        sign_up()
    elif first_decision == '2':
        sign_in()
    else:
        print('please choose a defined selection! just 1 or 2 :)')
        first_menu()


def find_id_from_mobile_number(mobile_number, role):
    if role == 'client':
        id_selected_query = "SELECT id from client where mobile_number = %s"
        cur.execute(id_selected_query, (mobile_number,))
        pass_id = cur.fetchone()
        pass_id = int(pass_id[0])
        conn.commit()
        return pass_id
    else:
        id_selected_query = "SELECT id from driver where mobile_number = %s"
        cur.execute(id_selected_query, (mobile_number,))
        dr_id = cur.fetchone()
        dr_id = int(dr_id[0])
        conn.commit()
        return dr_id


def sign_up():
    global c_counter
    # c_counter = 1
    global d_counter
    global add_counter
    cur.execute("SELECT max (id) from client")
    c_counter = cur.fetchone()
    c_counter = int(c_counter[0]) + 1
    conn.commit()
    # d_counter = 1
    cur.execute("SELECT max (id) from driver")
    d_counter = cur.fetchone()
    d_counter = int(d_counter[0]) + 1
    conn.commit()
    # add_counter = 1
    cur.execute("SELECT max (id) from most_used_address")
    add_counter = cur.fetchone()
    add_counter = int(add_counter[0]) + 1
    conn.commit()
    first_name = input('Enter your first name: ')
    last_name = input('Enter your last name: ')
    city_of_user = input('Enter name of city you live in: ')
    mobile_num = input('Enter your mobile phone number: ')
    print('Now, please let us known about your role\nchoose of these numbers')
    print('1.manager\t2.client\t3.driver')
    role = input('Enter number of your related role: ')
    cur.execute("SELECT city.id from city where city.name=%(name)s", {'name': city_of_user})
    user_city_id = cur.fetchone()
    conn.commit()
    if role == '2':
        cur.execute(
            "INSERT INTO Client (id,last_name,first_name,electronical_inventory,mobile_number,city_id)"
            "VALUES (%s,%s,%s,%s,%s,%s)", (c_counter, last_name, first_name, 0, mobile_num, user_city_id))
        conn.commit()
        c_counter += 1
        print('you have signed up successfully! '
              'now you will back to first menu')
        first_menu()
    elif role == '3':
        cur.execute(
            "INSERT INTO driver (id, mobile_number, first_name, last_name, city_id, activeness)"
            "VALUES (%s,%s,%s,%s,%s,%s)", (d_counter, mobile_num, first_name, last_name, user_city_id, 1)
        )
        conn.commit()
        d_counter += 1
        print('you have signed up successfully! '
              'now you will back to first menu')
        first_menu()


def sign_in():
    m_n = input('mobile_number: ')
    cur.execute("SELECT id from client where mobile_number = %(mobile_number)s", {'mobile_number': m_n})
    ckr = cur.fetchone()
    conn.commit()
    if ckr:
        print('signed in :)')
        dashboard()
    else:
        print('no user whit this mobile number :(\nplease sign up first')
        sign_up()


def dashboard():
    taken_role = input('please tell us what is your exact role: 1. driver\t2. passenger\t3. manager'
                       '\n your role is (please enter related number): ')
    if taken_role == '2':
        mob_num = input('please enter your cell phone number: ')
        print('here you can access to your Dashboard \n')
        print('some of information you may like to know: ')
        s_count_query = "SELECT count (distinct trip.id) as number_of_your_trips from client " \
                        "inner join trip on client.id=trip.passenger_id where client.mobile_number = %s"
        cur.execute(s_count_query, (mob_num,))
        count_trip = cur.fetchone()
        conn.commit()
        count_trip = int(count_trip[0])
        print('number of trips you have done is: ', count_trip)
        e_i_query = "SELECT electronical_inventory from client where mobile_number = %s"
        cur.execute(e_i_query, (mob_num,))
        inventory = cur.fetchone()
        conn.commit()
        inventory = int(inventory[0])
        print('your electrical inventory is: ', inventory)
        # a_s_query = "SELECT avg(numerical_score) from client inner join trip t on client.id = t.passenger_id " \
        #             "inner join score on score.trip_id = trip.id where score.c_or_d_score = 0 and mobile_number = %s"
        # cur.execute(a_s_query, (mob_num,))
        # score_avg = cur.fetchone()
        # conn.commit()
        # score_avg = int (score_avg[0])
        # print('average of your scores up to the present: ', score_avg)
        print('and also you can visit this service part: ')
        print('1. adding the most used addresses')
        print('2. editing user information')
        print('3. make a new trip')
        print('4. exit to sign in / up menu ')
        chosen_service = input('choose one of these services: ')
        pas_id = find_id_from_mobile_number(mob_num, 'client')
        conn.commit()
        if chosen_service == '1':
            wanted_adr = input('please enter your wanted address (a bit brief): ')
            cur.execute(
                "INSERT INTO most_used_address (id, passenger_id, address) "
                "VALUES (%s,%s,%s)", (add_counter, pas_id, wanted_adr)
            )
            conn.commit()
        elif chosen_service == '2':
            print('these are your information ')
            print('1. your first name')
            print('2. your last name ')
            print('3. your city (city which you live in)')
            print('4. exit to dashboard menu')
            chosen_change = input('which on of these information you want to chang? select a number: ')
            if chosen_change == '1':
                new_f_name = input('your new first name: ')
                cur.execute("update client set first_name = %s "
                            "where id = %s", (new_f_name, pas_id))
                conn.commit()
                print('your first name has been change successfully')
            elif chosen_change == '2':
                new_l_name = input('your new last name: ')
                cur.execute("update client set last_name = %s "
                            "where id = %s", (new_l_name, pas_id))
                conn.commit()
            elif chosen_change == '3':
                new_c_name = input('your name of new city you have located: ')
                cur.execute("SELECT city.id from city where city.name=%(name)s", {'name': new_c_name})
                conn.commit()
                new_c = cur.fetchone()
                new_c = int(new_c[0])
                conn.commit()
                cur.execute("update client set city_id = %s "
                            "where id = %s", (new_c, pas_id))
                conn.commit()
            elif chosen_change == '4':
                dashboard()
        elif chosen_service == '3':
            making_new_trip()
        elif chosen_service == 4:
            first_menu()
    elif taken_role == '1':
        cell_num = input('please enter your cell phone number: ')
        dr_id = find_id_from_mobile_number(cell_num, 'driver')
        conn.commit()
        print('here you can access to your Dashboard \n')
        print('some of information you may like to know: ')
        s_count_query = "SELECT count (distinct trip.id) as number_of_your_trips from driver " \
                        "inner join trip on driver.id=trip.driver_id where driver.mobile_number = %s"
        cur.execute(s_count_query, (cell_num,))
        count_trip = cur.fetchone()
        conn.commit()
        count_trip = int(count_trip[0])
        print('number of trips you have done is: ', count_trip)
        e_i_query = "SELECT electronical_inventory from driver where mobile_number = %s"
        cur.execute(e_i_query, (cell_num,))
        inventory = cur.fetchone()
        conn.commit()
        inventory = int(inventory[0])
        print('your electrical inventory is: ', inventory)
        # avg of score here
        print('if you want, you can choose any other service:\n')
        print('1.editing your profile')
        print('2.register your car')
        chosen_service = input('select the number of one of introduced services: ')
        if chosen_service == '1':
            print('these are your information ')
            print('1. your first name')
            print('2. your last name ')
            print('3. your city (city which you live in)')
            print('4. exit to dashboard menu')
            chosen_change = input('which on of these information you want to chang? select a number: ')
            if chosen_change == '1':
                new_f_name = input('your new first name: ')
                cur.execute("update driver set first_name = %s "
                            "where id = %s", (new_f_name, dr_id))
                conn.commit()
                print('your first name has been change successfully')
            elif chosen_change == '2':
                new_l_name = input('your new last name: ')
                cur.execute("update driver set last_name = %s "
                            "where id = %s", (new_l_name, dr_id))
                conn.commit()
            elif chosen_change == '3':
                new_c_name = input('your name of new city you have located: ')
                cur.execute("SELECT city.id from city where city.name=%(name)s", {'name': new_c_name})
                conn.commit()
                new_c = cur.fetchone()
                new_c = int(new_c[0])
                conn.commit()
                cur.execute("update driver set city_id = %s "
                            "where id = %s", (new_c, dr_id))
                conn.commit()
            elif chosen_change == '4':
                dashboard()
        elif chosen_service == '2':
            l_p = input('first, tell us license plate of your car: ')
            car_name = input('and now tell us what is your car name: ')
            cur.execute("select max(id) from car")
            new_car_id_finder = cur.fetchone()
            new_car_id_finder = new_car_id_finder[0]
            conn.commit()
            if new_car_id_finder is not None:
                new_car_id_finder = int(new_car_id_finder)
                cur.execute("insert into car (license_plate_of_car, id, driver_id, name) "
                            "values (%s,%s,%s,%s)", (l_p, new_car_id_finder+1, dr_id, car_name))
                conn.commit()
            else:
                cur.execute("insert into car (license_plate_of_car, id, driver_id, name) "
                            "values (%s,%s,%s,%s)", (l_p, 1, dr_id, car_name))
                conn.commit()
            print('your car has been registered successfully!')
            dashboard()
    elif taken_role == '3':
        print('here is the information of users: ')
        shower_query = "select first_name, last_name, mobile_number, electronical_inventory from client"
        cur.execute(shower_query)
        shower = cur.fetchall()
        conn.commit()
        for i in shower:
            print(i)
        score_query = "select client.id,client.first_name, client.last_name ,avg (numerical_score) from client inner join trip t on (client.id = t.passenger_id)" \
                      "inner join score s on (t.score_id = s.id)" \
                      "where c_or_d_score = 0" \
                      "group by client.id, client.first_name, client.last_name"
        print('________________________________________________\n\nthese are avg of numerical scores: ')
        cur.execute(score_query)
        score = cur.fetchall()
        conn.commit()
        for j in score:
            print(j)
        print('now please select one of these as your chosen service: ')
        print('1. adding a new predefined comment')
        print('2. activating or inactivating drivers')
        print('3. adding a new city')
        print('4. watching the list of all trips')
        chosen_manager_service = input('please enter number of your selected one: ')
        if chosen_manager_service == '2':
            id_taker = input('please enter id of driver you want to activate or inactivate: ')
            activate(id_taker)
        elif chosen_manager_service == '3':
            new_city_name = input('please enter name of city you want to add: ')
            city_adder(new_city_name)
        elif chosen_manager_service == '1':
            new_comment = input('please enter new statement you want to add to predefined sentences: ')
            new_comment_id_query = "select max(id) from predefined_sentences"
            cur.execute(new_comment_id_query)
            conn.commit()
            new_comment_id = cur.fetchone()
            new_comment_id = int(new_comment_id[0])
            cur.execute("insert into predefined_sentences (id, sentenc) values (%s,%s)", (new_comment_id+1, new_comment))
            conn.commit()
        elif chosen_manager_service == '4':
            trips_list_shower()


def activate(driver_id):
    print('which one do you want to do? 1. activate\t2. inactivate')
    action = input('select one of these: ')
    if action == 1:
        cur.execute("update driver set activeness = %s "
                    "where id = %s", (1, driver_id))
        conn.commit()
        print('this driver has been activated. ')
    elif action == '2':
        cur.execute("update driver set activeness = %s "
                    "where id = %s", (0, driver_id))
        conn.commit()
        print('this driver has been inactivated. ')


def city_adder(name):
    cur.execute("select max (id) from city")
    city_id_counter = cur.fetchone()
    conn.commit()
    city_id_counter = int(city_id_counter[0])
    cur.execute(
        "INSERT INTO city (id, name) "
        "VALUES (%s,%s)", (city_id_counter+1, name)
    )
    conn.commit()


def trips_list_shower():
    cur.execute("select client.first_name as passenger_name, client.last_name as passenger_last_name, "
                 "driver.first_name as driver_name , driver.last_name as driver_last_name, city.name, "
                 "price.real_price , price.paid_price, price.payment_methode "
                 "from trip inner join driver on (trip.driver_id = driver.id) "
                 "inner join client on (trip.passenger_id = client.id) "
                 "inner join price on (price.trip_id = trip.id) inner join city on (city.id = trip.city_id)")
    conn.commit()
    trips_list = cur.fetchall()
    print('these are: passenger name, passenger last name, driver name, driver last name, city, real price, paid price, payment methode')
    for iterator in trips_list:
        print(iterator)


def making_new_trip():
    global trip_counter
    cur.execute("select max(id) from trip")
    conn.commit()
    trip_id_generator = cur.fetchone()
    trip_id_generator = int(trip_id_generator[0])
    trip_id_generator = trip_id_generator + 1
    print('for making a new trip, we need these information:')
    source_address = input('1. your source address: ')
    cur.execute("select max(id) from s_address")
    conn.commit()
    s_id_selector = cur.fetchone()
    s_id_selector = int(s_id_selector[0])
    s_id_selector = s_id_selector + 1
    cur.execute("insert into s_address (id, trip_id, source) VALUES (%s, %s, %s)", (s_id_selector, trip_id_generator, source_address))
    print('here tell us how many destination address you want to choose? ')
    number_of_dests = input('number of your destinations is: ')
    dest_array = [None] * 5
    trip_counter = 0
    number_of_dests_inted = int(number_of_dests)
    while trip_counter < number_of_dests_inted:
        n_addr = input('your new dest address is: ')
        dest_array[trip_counter] = n_addr
        cur.execute("select max(id) from d_address")
        conn.commit()
        d_addr_id = cur.fetchone()
        d_addr_id = int(d_addr_id[0])
        d_addr_id = d_addr_id + 1
        cur.execute("insert into d_address (id, trip_id, dest) values (%s,%s,%s)", (d_addr_id, trip_id_generator, n_addr))
        conn.commit()
        trip_counter = trip_counter + 1


first_menu()
# trip_counter_query = "SELECT * from trip_counter('09107706735')"
# cur.execute(trip_counter_query)
# trip_count = cur.fetchone()
# conn.commit()
# trip_count = int(trip_count[0])
# print(trip_count)
